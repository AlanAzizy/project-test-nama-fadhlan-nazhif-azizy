"use client"; // Ensure this is a client component

import React, { useState } from "react";

interface PaginationProps {
  totalItems: number;
  itemsPerPage: number;
  onPageChange: (page: number) => void;
  setCurrentPage: (page: number) => void;
  currentPage: number;
}

const Pagination: React.FC<PaginationProps> = ({
  totalItems,
  itemsPerPage,
  onPageChange,
  currentPage,
  setCurrentPage,
}) => {
  const totalPages = Math.ceil(totalItems / itemsPerPage);

  const handlePageChange = (page: number) => {
    if (page < 1 || page > totalPages) return;
    setCurrentPage(page);
    onPageChange(page);
  };

  const pageNumbers = () => {
    const maxPageNumbersToShow = 5;
    let startPage = Math.max(
      1,
      currentPage - Math.floor(maxPageNumbersToShow / 2)
    );
    let endPage = startPage + maxPageNumbersToShow - 1;

    if (endPage > totalPages) {
      endPage = totalPages;
      startPage = Math.max(1, endPage - maxPageNumbersToShow + 1);
    }

    const pages = [];
    for (let i = startPage; i <= endPage; i++) {
      pages.push(i);
    }
    return pages;
  };

  return (
    <div className="flex justify-center mt-20">
      <nav className="flex space-x-1">
        <button
          onClick={() => handlePageChange(currentPage - 5)}
          className={`px-4 py-2 rounded ${
            currentPage <= 5
              ? "opacity-50 cursor-not-allowed"
              : "bg-none text-blue-500"
          }`}
          disabled={currentPage === 1}
        >
          <svg
            className="w-6 h-6 text-gray-800 dark:text-white"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            fill="none"
            viewBox="0 0 24 24"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="m17 16-4-4 4-4m-6 8-4-4 4-4"
            />
          </svg>
        </button>
        <button
          onClick={() => handlePageChange(currentPage - 1)}
          className={`px-4 py-2 rounded ${
            currentPage === 1
              ? "opacity-50 cursor-not-allowed"
              : "bg-none text-blue-500"
          }`}
          disabled={currentPage === 1}
        >
          <svg
            className="w-6 h-6 text-gray-800 dark:text-white"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            fill="none"
            viewBox="0 0 24 24"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="m14 8-4 4 4 4"
            />
          </svg>
        </button>
        {pageNumbers().map((page) => (
          <button
            key={page}
            onClick={() => handlePageChange(page)}
            className={`px-4 py-2 rounded font-bold ${
              currentPage === page
                ? "bg-[#f96500] text-white"
                : "bg-none text-black"
            }`}
          >
            {page}
          </button>
        ))}
        <button
          onClick={() => handlePageChange(currentPage + 1)}
          className={`px-4 py-2 rounded ${
            currentPage === totalPages
              ? "opacity-50 cursor-not-allowed"
              : "bg-none text-blue-500"
          }`}
          disabled={currentPage === totalPages}
        >
          <svg
            className="w-6 h-6 text-gray-800 dark:text-white"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            fill="none"
            viewBox="0 0 24 24"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="m10 16 4-4-4-4"
            />
          </svg>
        </button>
        <button
          onClick={() => handlePageChange(currentPage + 5)}
          className={`px-4 py-2 rounded ${
            currentPage > totalPages - 5
              ? "opacity-50 cursor-not-allowed"
              : "bg-none text-blue-500"
          }`}
          disabled={currentPage === totalPages}
        >
          <svg
            className="w-6 h-6 text-gray-800 dark:text-white"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            width="24"
            height="24"
            fill="none"
            viewBox="0 0 24 24"
          >
            <path
              stroke="currentColor"
              stroke-linecap="round"
              stroke-linejoin="round"
              stroke-width="2"
              d="m7 16 4-4-4-4m6 8 4-4-4-4"
            />
          </svg>
        </button>
      </nav>
    </div>
  );
};

export default Pagination;
