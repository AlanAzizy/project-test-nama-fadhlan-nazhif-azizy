import formatDateToIndonesian from "./formatDate";

type cardProps = {
  title: string;
  image: string;
  date: Date;
};

function Card({ title, date, image }: cardProps) {
  const newDate = new Date(date);
  console.log(image);
  return (
    <div className="w-52 h-80 bg-white rounded-lg shadow-lg">
      <img
        loading={"lazy"}
        className="w-full h-48 object-cover rounded-t-lg"
        src={image}
        alt="image"
      ></img>
      <div className="w-full p-2 h-32 flex-col flex justify-start gap-2">
        <div className="text-[#a1a1a1]">{formatDateToIndonesian(newDate)}</div>
        <text className="font-bold line-clamp-3">{title}</text>
      </div>
    </div>
  );
}

export default Card;
