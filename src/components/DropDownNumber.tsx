"use client"; // Add this directive at the top

import React, { useState } from "react";

interface Props {
  item: number[];
  setItemPerPage: (p: number) => void;
  setCurrentPage: (p: number) => void;
}

export default function Dropdown(props: Props) {
  const { item, setItemPerPage, setCurrentPage } = props;
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [clicked, setClicked] = useState<number | string | null>(item[0]);

  const toggle = () => {
    setIsOpen((old) => !old);
  };

  const transClass = isOpen ? "flex" : "hidden";

  return (
    <>
      <div
        className="relative border-[#ababab] border-2 rounded-2xl w-auto flex flex-row justify-between items-center px-4"
        onClick={toggle}
      >
        <div className="py-1 px-2 me-4" onClick={toggle}>
          <p onClick={toggle}>{clicked}</p>
        </div>
        <div
          className={`absolute -left-2 top-8 z-30 w-[150px] min-h-[100px] h-auto flex flex-col py-4 bg-[#f1f1f1] rounded-md  ${transClass}`}
        >
          {item &&
            item.map((e, index) => (
              <div
                key={index}
                className="px-4 py-1"
                onClick={() => {
                  setClicked(e);
                  setCurrentPage(1);
                  setItemPerPage(e);
                }}
              >
                {e}
              </div>
            ))}
        </div>
        <svg
          className="w-3 h-3 text-gray-800 dark:text-white"
          aria-hidden="true"
          xmlns="http://www.w3.org/2000/svg"
          fill="currentColor"
          viewBox="0 0 16 10"
        >
          <path d="M15.434 1.235A2 2 0 0 0 13.586 0H2.414A2 2 0 0 0 1 3.414L6.586 9a2 2 0 0 0 2.828 0L15 3.414a2 2 0 0 0 .434-2.179Z"></path>
        </svg>
      </div>
      {isOpen ? (
        <div
          className="fixed top-0 right-0 bottom-0 left-0 z-20 bg-black/40"
          onClick={toggle}
        ></div>
      ) : (
        <></>
      )}
    </>
  );
}
