"use client";

import { useContext, useEffect, useRef, useState } from "react";
import { useEventListener } from "usehooks-ts";
// import { postWithAuth } from "../api/api";
// import { toastError } from "./Toast";
import { usePathname } from "next/navigation";
import Link from "next/link";
// import LoadingPage from "./LoadingPage";
// import Logo from "/assets/suitmedia.png";
// import { UserContext } from "../Context/UserContext";
// import Cookies from "js-cookie";

import { MdOutlinePersonOutline } from "react-icons/md";

function Navbar() {
  const [navOpen, setNavOpen] = useState(false);
  const [stickyClass, setStickyClass] = useState("absolute bg-orange-primary");
  const location = usePathname();
  const [active, setActive] = useState(0);
  const [lastScrollTop, setLastScrollTop] = useState(0);
  const [navVisible, setNavVisible] = useState(true);
  //   const [isLoading, setIsLoading] = useState(false);

  const documentRef = useRef<Document | null>(
    typeof document !== "undefined" ? document : null
  );
  const onClickHamburger = (event: Event) => {
    let cekHamburger = true;
    const doc = document.getElementsByClassName("hamburger");
    for (let index = 0; index < doc.length; index++) {
      cekHamburger = cekHamburger && event.target != doc[index];
    }
    if (cekHamburger) {
      setNavOpen(false);
    }
  };
  useEventListener("click", onClickHamburger, documentRef);

  useEffect(() => {
    if (location == "/") {
      setActive(0);
    } else if (location.includes("/work")) {
      setActive(1);
    } else if (location == "/about") {
      setActive(2);
    } else if (location == "/services") {
      setActive(3);
    } else if (location == "/ideas") {
      setActive(4);
    } else if (location == "/careers") {
      setActive(5);
    } else if (location == "/contacts") {
      setActive(6);
    } else {
      setActive(-1);
    }
  }, [location]);

  const stickNavbar = () => {
    const scrollTop = window.scrollY || document.documentElement.scrollTop;
    if (scrollTop > lastScrollTop) {
      setNavVisible(false);
      setNavOpen(false);
    } else {
      setNavVisible(true);
    }
    setLastScrollTop(scrollTop <= 0 ? 0 : scrollTop);

    const header = document.querySelector("nav");
    if (header != null) {
      window.scrollY > header.offsetTop
        ? setStickyClass(
            "fixed bg-orange-primary bg-opacity-60 backdrop-blur-sm"
          )
        : setStickyClass("absolute bg-orange-primary");
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", stickNavbar);

    return () => {
      window.removeEventListener("scroll", stickNavbar);
    };
  }, [lastScrollTop]);

  //   const navigator = useNavigate();
  //   const logout = async () => {
  //     setIsLoading(true);
  //     try {
  //       await postWithAuth("logout", null, user?.token ?? "");
  //     } catch (error) {
  //       toastError((error as any).response.data.meta.message as string);
  //     } finally {
  //       Cookies.remove("access_token");
  //       navigator("/login");
  //       setIsLoading(false);
  //     }
  //   };

  return (
    <>
      <nav
        className={`${navOpen ? "fixed bg-orange-primary" : stickyClass} ${
          navVisible ? "translate-y-0" : "-translate-y-full"
        } transition-transform duration-300 z-50 flex h-[76px] lg:h-[84px] w-full items-center justify-between text-dark-maintext px-4 lg:px-14 font-poppins`}
      >
        <button
          type="button"
          className={`hamburger absolute z-10 h-[40px] w-[40px] cursor-pointer lg:hidden`}
          onClick={() => setNavOpen(!navOpen)}
        >
          <span
            className={`${
              navOpen
                ? "top-[1.2em] h-[2px] rotate-[135deg] transition"
                : "top-[0.7em] h-[3px]"
            } hamburger line absolute left-0 right-0 mx-auto h-[3px] w-[20px] rounded-xl bg-white duration-300 ease-in-out`}
          ></span>
          <span
            id="span2"
            className={`${
              navOpen ? "h-[2px] scale-0 transition" : "top-[1.2em] h-[3px]"
            } hamburger line absolute left-0 right-0 mx-auto h-[3px] w-[20px] rounded-xl bg-white duration-300 ease-in-out`}
          ></span>
          <span
            id="span3"
            className={`${
              navOpen
                ? "top-[1.2em] h-[2px] rotate-45 transition"
                : "top-[1.7em] h-[3px]"
            } hamburger line absolute left-0 right-0 mx-auto h-[3px] w-[20px] rounded-xl bg-white duration-300 ease-in-out`}
          ></span>
        </button>
        <Link href="/" className="mx-auto lg:mx-0">
          <img
            src={"/assets/suitmedia.png"}
            alt="Carport"
            className="h-12 object-contain"
          />
        </Link>
        <div
          className={`${
            navOpen ? "translate-x-0" : "-translate-x-full lg:translate-x-0"
          } pt-20 absolute left-0 top-0 h-screen w-[70%] sm:w-[50%] md:w-[40%] bg-orange-primary shadow-lg duration-300 ease-in-out lg:static lg:block lg:h-auto lg:w-auto lg:bg-transparent lg:pt-0 lg:shadow-none`}
        >
          <div className="flex flex-col gap-4 px-7 lg:mt-0 lg:flex-row lg:items-start lg:gap-8 xl:gap-[48px] lg:px-0">
            <div className="flex flex-col w-min group">
              <Link
                href="/work"
                className={`${
                  active == 1 ? "font-bold" : "font-medium"
                } text-white text-[16px] lg:text-[20px] group-hover:font-bold truncate`}
              >
                Work
              </Link>
              <div
                className={`${
                  active == 1 ? "scale-100" : "scale-0"
                } h-1 bg-white ease-in-out duration-300`}
              ></div>
            </div>
            <div className="flex flex-col w-min group">
              <Link
                href="/about"
                className={`${
                  active == 2 ? "font-bold" : "font-medium"
                } text-white text-[16px] lg:text-[20px] group-hover:font-bold truncate`}
              >
                About
              </Link>
              <div
                className={`${
                  active == 2 ? "scale-100" : "scale-0"
                } h-1 bg-white ease-in-out duration-300`}
              ></div>
            </div>
            <div className="flex flex-col w-min group">
              <Link
                href="/services"
                className={`${
                  active == 3 ? "font-bold" : "font-medium"
                } text-white text-[16px] lg:text-[20px] group-hover:font-bold truncate`}
              >
                Services
              </Link>
              <div
                className={`${
                  active == 3 ? "scale-100" : "scale-0"
                } h-1 bg-white ease-in-out duration-300`}
              ></div>
            </div>
            <div className="flex flex-col w-min group">
              <Link
                href="/ideas"
                className={`${
                  active == 4 ? "font-bold" : "font-medium"
                } text-white text-[16px] lg:text-[20px] group-hover:font-bold truncate`}
              >
                Ideas
              </Link>
              <div
                className={`${
                  active == 4 ? "scale-100" : "scale-0"
                } h-1 bg-white ease-in-out duration-300`}
              ></div>
            </div>
            <div className="flex flex-col w-min group">
              <Link
                href="/careers"
                className={`${
                  active == 5 ? "font-bold" : "font-medium"
                } text-white text-[16px] lg:text-[20px] group-hover:font-bold truncate`}
              >
                Careers
              </Link>
              <div
                className={`${
                  active == 5 ? "scale-100" : "scale-0"
                } h-1 bg-white ease-in-out duration-300`}
              ></div>
            </div>
            <div className="flex flex-col w-min group lg:hidden">
              <Link
                href="/contacts"
                className={`${
                  active == 6 ? "font-bold" : "font-medium"
                } text-white text-[16px] lg:text-[20px] group-hover:font-bold truncate`}
              >
                Contacts
              </Link>
              <div
                className={`${
                  active == 6 ? "scale-100" : "scale-0"
                } h-1 bg-white ease-in-out duration-300`}
              ></div>
            </div>
            <div className="hidden lg:flex flex-col w-min group">
              <Link
                href="/contacts"
                className={`${
                  active == 6 ? "font-bold" : "font-medium"
                } text-white text-[16px] lg:text-[20px] group-hover:font-bold truncate`}
              >
                Contacts
              </Link>
              <div
                className={`${
                  active == 6 ? "scale-100" : "scale-0"
                } h-1 bg-white ease-in-out duration-300`}
              ></div>
            </div>
          </div>
        </div>
      </nav>
      <div
        className={`${
          navOpen ? "block lg:hidden" : "hidden"
        } fixed z-10 bg-black h-screen w-full opacity-50`}
      ></div>
    </>
  );
}

export default Navbar;
