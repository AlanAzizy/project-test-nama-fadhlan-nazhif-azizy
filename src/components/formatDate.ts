export default function formatDateToIndonesian(date : Date) {
    // Pastikan input adalah objek Date
    const dateObj = new Date(date);
    
    // Gunakan Intl.DateTimeFormat dengan lokal 'id-ID' untuk format Indonesia
    const formattedDate = new Intl.DateTimeFormat('id-ID', {
      day: 'numeric',
      month: 'long',
      year: 'numeric'
    }).format(dateObj);
  
    return formattedDate;
  }