export default function Work() {
  return (
    <main className="flex min-h-screen items-center justify-center p-24 ">
      <p className="text-[40px]">This is the work page</p>
    </main>
  );
}
