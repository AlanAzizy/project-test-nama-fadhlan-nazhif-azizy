"use client"; // Ensure this is a client component

import Card from "@/components/Card";
import DropDownNumber from "@/components/DropDownNumber";
import DropDownString from "@/components/DropDownString";
import Image from "next/image";
import { useState, useEffect } from "react";
import axios from "axios";

const numberItems: number[] = [10, 20, 50];
const sortItems: string[] = ["Newest", "Oldest"];
import Pagination from "@/components/Pagination";
import { AnyAaaaRecord } from "dns";

type image = {
  id: number;
  mime: string;
  file_name: string;
  url: string;
};

type idea = {
  id: number;
  slug: string;
  title: string;
  published_at: Date;
  deleted_at: Date;
  created_at: Date;
  updated_at: Date;
  small_image: image;
  medium_image: image;
};

const items = Array.from({ length: 100 }, (_, i) => `Item ${i + 1}`);

const itemsPerPage = 10;

export default function Ideas() {
  const [currentPage, setCurrentPage] = useState(1);
  const [itemPerPage, setItemPerPage] = useState<number>(10);
  const [order, setOrder] = useState<string>("Newest");

  const handlePageChange = (page: number) => {
    setCurrentPage(page);
  };

  const [allIdeas, setAllIdeas] = useState<idea[]>();

  const [ideas, setIdeas] = useState<idea[]>();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await axios.get(
          "https://suitmedia-backend.suitdev.com/api/ideas",
          {
            params: {
              "page[number]": currentPage,
              "page[size]": itemPerPage,
              "append[]": ["small_image", "medium_image"],
              sort: order == "Newest" ? "-published_at" : "published_at",
            },
            headers: {
              Accept: "application/json",
            },
          }
        );
        const data = response.data.data as idea[];
        for (let i = 0; i < data.length; i++) {
          data[i].medium_image = response.data.data[i].medium_image[0] as image;
          data[i].small_image = response.data.data[i].small_image[0] as image;
          console.log(data[i].small_image);
        }
        setIdeas(data);
      } catch (error: any) {
        setError(error.message);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [currentPage, itemPerPage, order]);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const currentItems = items.slice(startIndex, startIndex + itemsPerPage);

  return (
    <main className="flex min-h-screen flex-col items-center justify-between pt-[76px] lg:pt-[84px] h-auto pb-20 bg-slate-50">
      <div className="w-full h-fit relative overflow-hidden">
        <div className="relative w-full h-[34vw] sm:h-[28vw] lg:h-[24vw] xl:h-[21vw]">
          <div
            className="w-full h-[34vw] sm:h-[28vw] lg:h-[24vw] xl:h-[21vw] bg-[url('/assets/background-hero.jpg')] bg-center absolute top-0 overflow-hidden"
            style={{
              WebkitClipPath: "polygon(100% 65%, 100% 0, 0 0, 0 100%)",
              clipPath: "polygon(100% 65%, 100% 0, 0 0, 0 100%)",
            }}
          >
            <div className="bg-black bg-opacity-30 w-full h-full"></div>
          </div>
          <div className="w-full h-full flex justify-center items-center relative">
            <div className="flex flex-col gap-2 items-center">
              <h1 className="text-white leading-none text-[24px] sm:text-[28px] md:text-[32px] lg:text-[36px]">
                Ideas
              </h1>
              <h2 className="text-white text-[14px] md:text-[16px] lg:text-[20px] leading-none">
                Where our all great things begin
              </h2>
            </div>
            {/* <div
              className="w-full h-full relative overflow-hidden"
              data-aos="fade-down"
            >
              <h2 className="text-[32px] sm:text-[40px] md:text-[48px] lg:text-[56px] xl:text-[88px] text-white font-poppins font-bold text-right relative z-[2]">
                Membership
              </h2>
              <h2 className="text-[32px] sm:text-[40px] md:text-[48px] lg:text-[56px] xl:text-[88px] text-[#2F2F2F] font-poppins font-bold text-right absolute top-[2px] left-[2px] opacity-35">
                Membership
              </h2>
            </div> */}
          </div>
        </div>
        <div className="md:w-5/6 w-full min-h-96 p-5 md:mx-auto flex flex-col items-center">
          <div className="flex md:flex-row flex-col w-full justify-center flex-wrap items-center py-5 px-0 my-10">
            <text className="lg:w-1/2 md:w-1/3 self-start md:self-center ps-6 md:ps-0">{`showing ${
              (currentPage - 1) * itemPerPage + 1
            } - ${
              (currentPage - 1) * itemPerPage + (ideas ? ideas?.length : 0)
            } of 274`}</text>
            <div className="lg:w-1/2 md:w-2/3 flex flex-row justify-evenly md:justify-between ">
              <div className="w-5/12 flex flex-row items-center justify-end gap-2">
                <div className="md:w-1/2 lg:w-auto">Show per page :</div>
                <DropDownNumber
                  item={numberItems}
                  setItemPerPage={setItemPerPage}
                  setCurrentPage={setCurrentPage}
                />
              </div>
              <div className="w-5/12 flex flex-row items-center justify-end gap-2">
                <div className="md:w-1/2 lg:w-auto">Sort by : </div>
                <DropDownString item={sortItems} setItemPerPage={setOrder} />
              </div>
            </div>
          </div>
          <div className="flex flex-row gap-8 md:justify-between justify-center flex-wrap">
            {!loading &&
              ideas &&
              ideas.map((idea, index) => (
                <Card
                  key={index}
                  title={idea.title}
                  image={idea.medium_image.url}
                  date={idea.published_at}
                />
              ))}
          </div>
        </div>
        <Pagination
          totalItems={270}
          itemsPerPage={itemPerPage}
          onPageChange={handlePageChange}
          currentPage={currentPage}
          setCurrentPage={setCurrentPage}
        />
      </div>
      {/* <div className="z-10 w-full max-w-5xl items-center justify-between font-mono text-sm lg:flex">
        <p className="fixed left-0 top-0 flex w-full justify-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto  lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30">
          Get started by editing&nbsp;
          <code className="font-mono font-bold">src/app/page.tsx</code>
        </p>
        <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:size-auto lg:bg-none">
          <a
            className="pointer-events-none flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0"
            href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            target="_blank"
            rel="noopener noreferrer"
          >
            By{" "}
            <Image
              src="/vercel.svg"
              alt="Vercel Logo"
              className="dark:invert"
              width={100}
              height={24}
              priority
            />
          </a>
        </div>
      </div>

      <div className="relative z-[-1] flex place-items-center before:absolute before:h-[300px] before:w-full before:-translate-x-1/2 before:rounded-full before:bg-gradient-radial before:from-white before:to-transparent before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-full after:translate-x-1/3 after:bg-gradient-conic after:from-sky-200 after:via-blue-200 after:blur-2xl after:content-[''] before:dark:bg-gradient-to-br before:dark:from-transparent before:dark:to-blue-700 before:dark:opacity-10 after:dark:from-sky-900 after:dark:via-[#0141ff] after:dark:opacity-40 sm:before:w-[480px] sm:after:w-[240px] before:lg:h-[360px]">
        <Image
          className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
          src="/next.svg"
          alt="Next.js Logo"
          width={180}
          height={37}
          priority
        />
      </div>

      <div className="mb-32 grid text-center lg:mb-0 lg:w-full lg:max-w-5xl lg:grid-cols-4 lg:text-left">
        <a
          href="https://nextjs.org/docs?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className="mb-3 text-2xl font-semibold">
            Docs{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className="m-0 max-w-[30ch] text-sm opacity-50">
            Find in-depth information about Next.js features and API.
          </p>
        </a>

        <a
          href="https://nextjs.org/learn?utm_source=create-next-app&utm_medium=appdir-template-tw&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className="mb-3 text-2xl font-semibold">
            Learn{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className="m-0 max-w-[30ch] text-sm opacity-50">
            Learn about Next.js in an interactive course with&nbsp;quizzes!
          </p>
        </a>

        <a
          href="https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className="mb-3 text-2xl font-semibold">
            Templates{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className="m-0 max-w-[30ch] text-sm opacity-50">
            Explore starter templates for Next.js.
          </p>
        </a>

        <a
          href="https://vercel.com/new?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className="mb-3 text-2xl font-semibold">
            Deploy{" "}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className="m-0 max-w-[30ch] text-balance text-sm opacity-50">
            Instantly deploy your Next.js site to a shareable URL with Vercel.
          </p>
        </a>
      </div> */}
    </main>
  );
}
